from django.conf.urls import url

#from . import views
#from .views import home

from .views import *



urlpatterns = [

    url(r'^login/', login_v, name="login"),
    url(r'^register/', SignUp.as_view(template_name='signup.html'), name="register"),
]
